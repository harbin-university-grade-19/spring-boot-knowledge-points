package com.neuedu.boot.security.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.neuedu.boot.security.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserMapperTest {

    @Autowired
    UserMapper userMapper;


    @Test
    void test(){
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("username","admin");
        User user = userMapper.selectOne(wrapper);
        System.out.println("user = " + user);

    }


}