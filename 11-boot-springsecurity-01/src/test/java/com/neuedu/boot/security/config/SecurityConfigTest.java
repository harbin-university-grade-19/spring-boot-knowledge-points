package com.neuedu.boot.security.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SecurityConfigTest {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    void passwordEncoder() {
        String rawPassword = "123456";
        String encodePassword = passwordEncoder.encode(rawPassword);
        System.out.println("encodePassword = " + encodePassword);
    }
}