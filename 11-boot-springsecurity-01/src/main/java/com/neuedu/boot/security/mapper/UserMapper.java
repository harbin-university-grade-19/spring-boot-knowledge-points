package com.neuedu.boot.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.neuedu.boot.security.entity.User;

public interface UserMapper extends BaseMapper<User> {
}
