package com.neuedu.boot.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.neuedu.boot.security.entity.User;

/**
 * <p>
 * 医生-用户信息 服务类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface IUserService extends IService<User> {


}
