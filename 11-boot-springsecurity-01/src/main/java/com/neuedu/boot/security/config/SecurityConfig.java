package com.neuedu.boot.security.config;

import com.neuedu.boot.security.security.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Bean
    PasswordEncoder passwordEncoder(){

        return  new BCryptPasswordEncoder();
    }


    @Autowired
    MyUserDetailsService userDetailsService;


    //配置认证管理器
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        //构造一个超级管理员的角色
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("Admin");

        //$2a$10$Jv1lgSebvvdieROLHeZqR.BzuhxwmbnhtVaAe7YaY6kIEIn2YBu8y
        //noop
//        auth.inMemoryAuthentication().withUser("admin").password("{noop}888888").authorities(authorities);

        //$2a$10$Jv1lgSebvvdieROLHeZqR.BzuhxwmbnhtVaAe7YaY6kIEIn2YBu8y
//        String password =  passwordEncoder.encode("666666");
//        System.out.println("password = " + password);
//
//        auth.inMemoryAuthentication().withUser("admin").password(password).authorities(authorities);

        //888888 -->转换成密文


        //设置一个类，用户查询数据库
        auth.userDetailsService(userDetailsService);

    }


    /**
     * 在IOC容器中定义Bean AuthenticationManager
     * @return
     * @throws Exception
     */
    @Bean()
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        //关闭
//        http.csrf().disable();
//        //关闭session策略
//        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
//        //允许匿名访问
//        http.authorizeRequests().antMatchers("/user/login").anonymous();
//        //其他请求都需要验证
//        http.authorizeRequests().anyRequest().authenticated();


        http.
                //关闭 csrf
                csrf().disable().
                //关闭session策略
                sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
                //允许 /user/login匿名访问
                and().authorizeRequests().antMatchers("/user/login").anonymous()
                //除了以上配置，其他的请求都需要登录
                .anyRequest().authenticated();

    }
}
