package com.neuedu.boot.security;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.neuedu.boot.security.mapper")
public class SecurityApp8081 {
    public static void main(String[] args) {
        SpringApplication.run(SecurityApp8081.class,args);
    }
}
