package com.neuedu.boot.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.neuedu.boot.security.entity.User;
import com.neuedu.boot.security.mapper.UserMapper;
import com.neuedu.boot.security.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医生-用户信息 服务实现类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {


}
