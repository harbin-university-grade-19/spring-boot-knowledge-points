package com.neuedu.boot.security.security;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.neuedu.boot.security.entity.User;
import com.neuedu.boot.security.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class MyUserDetailsService  implements UserDetailsService {

    @Autowired
    IUserService userService;

    /**
     * 根据用户名查询数据库的身份信息
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //根据用户名，查询用户身份信息，
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("username",username);
        User user = userService.getOne(wrapper);

        //如果不存在用户名，不需要往下走了，
        if(user == null){
            throw new RuntimeException("用户名或者密码不正确，再想想吧");
        }

        //通过用户名能够查询到一个用户
//        return new com.neuedu.boot.security.security.LoginUser(user);
        return new LoginUser(user);
    }
}
