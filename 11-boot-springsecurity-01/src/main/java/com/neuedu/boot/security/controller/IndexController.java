package com.neuedu.boot.security.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {


    @RequestMapping("/index")
    String index(){
        return "欢迎访问  后端的资源";
    }


}
