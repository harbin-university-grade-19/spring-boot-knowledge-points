package com.neuedu.boot.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Getter
@Setter
@TableName("stu_teacher_rela")
public class StuTeacherRela implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer teacherId;

    private Integer stuId;


}
