package com.neuedu.boot.security.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 医生-用户信息
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 * sys_user
 */
@Getter
@Setter
@ToString
//@TableName("sys_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 医生id
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 真实姓名
     */
    private String realname;

    /**
     * 电话号码
     */
    private String telephone;

    /**
     * id
     */
    private Integer deptId;

    /**
     * 医生类型
     */
    private Integer userType;

    /**
     * 最后登录时间
     */
    private LocalDateTime lastlogin;

    /**
     * 是否有效，1 有效，0 失效
     */
    private Integer active;

    /**
     * 创建时间
     */
    private LocalDateTime createtime;


}
