package com.neuedu.boot.security.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 菜单
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Getter
@Setter
@ToString
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 菜单id
     */
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Integer menuId;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 菜单url
     */
    private String url;

    /**
     * 上级菜单id
     */
    private Integer parentId;

    /**
     * 是否有效，1 有效，0 失效
     */
    private Integer active;

    /**
     * 创建时间
     */
    private LocalDateTime createtime;


    //是不是表里面真实存在的字段吗？ 只要加上属性，MyBatisplus默认认为是数据库中的字段
    @TableField(exist = false) // 如果不添加TableField 影响单表的CRUD
    private List<Menu> children = new ArrayList();

    @TableField(exist = false) // 如果不添加TableField 影响单表的CRUD
    private boolean hasChildren=true;

}
