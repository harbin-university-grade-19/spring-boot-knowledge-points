package com.neuedu.wechat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @auth 金山老师
 * title:   书籍代码
 * creatime: 2022-06-13 10:47
 * 描述 : 微信小程序的后端启动类
 */
@SpringBootApplication
public class WechatApp {
    public static void main(String[] args) {
        SpringApplication.run(WechatApp.class,args);
    }
}
