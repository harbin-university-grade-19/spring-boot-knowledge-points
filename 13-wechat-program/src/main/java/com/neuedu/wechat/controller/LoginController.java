package com.neuedu.wechat.controller;

import com.neuedu.wechat.common.CommonResult;
import com.neuedu.wechat.entity.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @auth 金山老师
 * title:   书籍代码
 * creatime: 2022-06-13 10:49
 * 描述 : 模拟登陆
 */
@RestController
public class LoginController {


    @RequestMapping("/login")
    CommonResult login(@RequestBody  User user){

        if("admin".equals(user.getUsername()) && "123456".equals(user.getPassword())){
            return CommonResult.success(new User(1,"admin","123456","管理员"));
        }else{
            return CommonResult.failed("用户名或者密码不正确");
        }

    }






}
