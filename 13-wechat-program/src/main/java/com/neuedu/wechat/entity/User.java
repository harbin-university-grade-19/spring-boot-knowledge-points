package com.neuedu.wechat.entity;

/**
 * @auth 金山老师
 * title:   书籍代码
 * creatime: 2022-06-13 10:49
 * 描述 :
 */
public class User {

    private Integer id ;
    private String username;
    private String password;
    private String realname;


    public User(Integer id, String username, String password, String realname) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.realname = realname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }
}
