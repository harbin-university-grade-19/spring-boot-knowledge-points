<%--
  Created by IntelliJ IDEA.
  User: jshand
  Date: 2022-04-01
  Time: 11:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

已上传文件:
<table width="100%" cellpadding="0" cellspacing="0" border="1">
    <tr>
        <th>序号</th>
        <th>文件名</th>
        <th>文件大小</th>
        <th>操作</th>
    </tr>
    <c:forEach items="${files}" var="file" varStatus="stat">
        <tr>
            <td>${stat.count}</td>
            <td>${file.getName()}</td>
            <td>${file.length()}</td>
            <td><a href="/download?filename=${file.getName()}">下载</a></td>
        </tr>
    </c:forEach>


</table>




</body>
</html>
