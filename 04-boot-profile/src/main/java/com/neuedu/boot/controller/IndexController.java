package com.neuedu.boot.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    /**
     * http://127.0.0.1:8080/test
     * http://localhost:8080/test
     * @return
     */
    @RequestMapping("/test")
    String test(){


        return "测试controller";
    }


}
