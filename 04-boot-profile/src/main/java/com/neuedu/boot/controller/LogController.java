package com.neuedu.boot.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class LogController {


    //com.neuedu.boot.controller.LogController  :开关
    //使用@Slf4j注解代替logger对象的声明
//    Logger logger = LoggerFactory.getLogger(LogController.class);


    @RequestMapping("/log")
    String log(){

        log.trace("打印【trace】日志");
        log.debug("打印【debug】日志");
        log.info("打印【info】日志");
        log.warn("打印【warn】日志");
        log.error("打印【error】日志");

        return "success";
    }

}
