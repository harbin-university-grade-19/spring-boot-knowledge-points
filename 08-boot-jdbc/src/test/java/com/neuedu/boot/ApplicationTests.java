package com.neuedu.boot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@SpringBootTest
class ApplicationTests {

    //从IOC容器中获取Bean
    @Autowired
    DataSource dataSource;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Test
    void contextLoads() throws SQLException {
        Connection connection = dataSource.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("select count(1) count from user");
        rs.next();

        int count = rs.getInt("count");
        System.out.println("count = " + count);

        rs.close();
        stmt.close();
        connection.close();

    }

    @Test
    void userTemplate(){

        String sql = "select count(1) count from user";
        Integer count = jdbcTemplate.queryForObject(sql, Integer.class);
        System.out.println("JDBC count = " + count);

    }



}
