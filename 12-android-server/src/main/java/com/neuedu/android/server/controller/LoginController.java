package com.neuedu.android.server.controller;

import com.neuedu.android.server.CommonResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @auth 金山老师
 * title:   书籍代码
 * creatime: 2022-06-02 9:20
 * 描述 : 用于Android的登录
 */
@RestController
public class LoginController {


    /**
     * http://127.0.0.1:3146/android-server/loginXml?username=admin&password=123456
     * @param username
     * @param password
     * @return
     */
    //用户返回数据 xml
    @RequestMapping("/loginXml")
    String loginXml(String username, String password) {

        //TODO 经过数据的的判断,xxxxxxx
        String code = "500";
        String msg = "";
        if ("admin".equals(username) && "123456".equals(password)) {
            code = "200";
        } else {
            msg = "用户名或密码不正确";
        }

        String result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "                <result>\n" +
                "                    <code>" + code + "</code>\n" +
                "                    <msg>" + msg + "</msg>\n" +
                "                </result>";

        return result;
    }


    @RequestMapping("/loginJSON")
    Object loginJSON(String username, String password) {

        //TODO 经过数据的的判断,xxxxxxx

        if ("admin".equals(username) && "123456".equals(password)) {
            return CommonResult.success(true);
        } else {
            return CommonResult.failed("用户名或密码不正确");
        }

    }




}
