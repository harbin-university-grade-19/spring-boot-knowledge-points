package com.neuedu.android.server.controller;

import com.neuedu.android.server.CommonResult;
import com.neuedu.android.server.Passanger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @auth 金山老师
 * title:   书籍代码
 * creatime: 2022-06-02 15:02
 * 描述 :
 */
@RestController
@RequestMapping("/passanger")
public class PassangerController {

    List<Passanger> list = new ArrayList();
    String names[] = {"刘备","关羽","松江","诸葛亮","乾隆","夏侯惇","周瑜","马谡","赵云","李逵"};
    String[] types = {"成人","学生","儿童"};





    public PassangerController(){
        Random rnd = new Random();
        //模拟从数据库查询
        for (int i = 0; i < names.length; i++) {
            list.add(new Passanger(i+1,names[i],Math.abs(rnd.nextLong())+"",types[rnd.nextInt(3)],Math.abs(rnd.nextLong())+""));
        }

    }


    /**
     * http://192.168.3.3:3146/android-server/passanger/list
     * @return
     */
    @RequestMapping("/list")
    public CommonResult list(){

        //TODO 应该调用Service
        return CommonResult.success(list);
    }







}
