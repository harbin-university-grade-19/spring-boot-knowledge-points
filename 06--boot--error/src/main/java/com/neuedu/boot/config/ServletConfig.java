package com.neuedu.boot.config;

import com.neuedu.boot.servletapi.filter.MyFilter;
import com.neuedu.boot.servletapi.listener.MyListener;
import com.neuedu.boot.servletapi.servlet.MyServlet;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServletConfig {

    //声明Servlet的Bean
    @Bean
    ServletRegistrationBean<MyServlet> initServlet(){
        ServletRegistrationBean<MyServlet> servletRegistrationBean = new ServletRegistrationBean();
        //声明注册的Servlet类
        servletRegistrationBean.setServlet(new MyServlet());
        //注册Servlet映射的路径
        servletRegistrationBean.addUrlMappings("/Servlet2");
        return servletRegistrationBean;//自动声明Servlet

    }


    /**
     * 声明Filter
     * @return
     */
    @Bean
    FilterRegistrationBean<MyFilter> initFilter(){

        FilterRegistrationBean<MyFilter> filterRegistrationBean = new FilterRegistrationBean();
        //声明注册的Filter类
        filterRegistrationBean.setFilter(new MyFilter());
        //注册Filter映射的路径
        filterRegistrationBean.addUrlPatterns("/Servlet2");
        return filterRegistrationBean;//自动声明Servlet

    }


    /**
     * 声明Listener
     * @return
     */
    @Bean
    ServletListenerRegistrationBean getServletListenerRegistrationBean(){
        ServletListenerRegistrationBean registrationBean = new ServletListenerRegistrationBean();
        registrationBean.setListener(new MyListener());
        return registrationBean;
    }







}
