package com.neuedu.boot.servletapi.listener;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

//@WebListener
public class MyListener implements ServletContextListener{

    public MyListener() {
    }

    /**
     *  应用程序创建一个全局的ServletContext 对象 application、session、request、page
     * @param sce
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("\r\n\r\n\r\n程序已启动\r\n\r\n\r\n\r\n");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("\r\n\r\n\r\n应用程序已销毁\r\n\r\n\r\n\r\n");
    }


}
