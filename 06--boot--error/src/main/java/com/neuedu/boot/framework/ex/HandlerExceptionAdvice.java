package com.neuedu.boot.framework.ex;


import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 处理全局的异常处理
 */

@ControllerAdvice
public class HandlerExceptionAdvice {

    @ExceptionHandler(value = Exception.class )
    @ResponseBody
    String resolveException(HttpServletRequest request, HttpServletResponse response, Exception ex  ){

        System.out.println(ex.getMessage());
        return "{status:'unup',msg:'全局的处理，Controller出错了'}";
    }
}
