package com.neuedu.boot.entity;

import lombok.Data;

import javax.validation.constraints.Size;

@Data  // setter getter toString ,eqausl hashcode
public class User {

    private Integer id;

    @Size(max = 10,min = 3,message = "{user.username.length}")
    private String username;

    private String password;


}
