package com.neuedu.boot.controller;

import com.neuedu.boot.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/user")
public class UserController {


    /**
     * http://127.0.0.1:8080/user/save?id=10&username=abcdef&password=123456
     * @param user
     * @param br
     * @return
     */
    @GetMapping("/save")
    Object save(@Validated  User user, BindingResult br){

        log.info(user.toString());

        if(br.getErrorCount()>0){
            return br.getAllErrors();
        }

        return user;
    }

}
