package com.neuedu.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class MyController {


    @RequestMapping("/create_error")
    String myreq(){
        int rest = 0/0;
        return "";
    }


    /**
     * 处理当前Controller的 异常处理方法
     * @param request
     * @param response
     * @param ex
     * @return
     */
//    @ExceptionHandler(value={RuntimeException.class,NullPointerException.class})
    @ExceptionHandler(value=Exception.class)
    @ResponseBody
    String resolveException(HttpServletRequest request, HttpServletResponse response, Exception ex  ){

        System.out.println(ex.getMessage());
        return "{status:'unup',msg:'出错了'}";
    }


}
