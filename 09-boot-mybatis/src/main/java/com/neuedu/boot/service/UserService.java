package com.neuedu.boot.service;

import com.neuedu.boot.dao.UserMapper;
import com.neuedu.boot.entity.User;
import com.neuedu.boot.entity.UserExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserMapper userMapper;


    /**
     * 模糊查询获取表格数据
     * @param user
     * @return
     */
    public List list(User user){

        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        //如果用户名存在则模糊查询
        if(user !=null && !ObjectUtils.isEmpty(user.getUsername())){
            criteria.andUsernameLike("%"+user.getUsername()+"%");
        }
        return userMapper.selectByExample(example);
    }


    /**
     *   //user有主见字段 更新，否则是添加
     * @param user
     * @return
     */
    public boolean save(User user) {

        if(ObjectUtils.isEmpty(user.getUserId())){
            return userMapper.insertSelective(user)>0;
        }else{
            return userMapper.updateByPrimaryKeySelective(user)>0;
        }

        
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    public User queryById(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }

    public boolean removeById(Integer id) {
        return userMapper.deleteByPrimaryKey(id)>0;
    }
}
