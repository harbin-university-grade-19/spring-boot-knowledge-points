package com.neuedu.boot.dao;

import com.neuedu.boot.entity.RoleMenuExample;
import com.neuedu.boot.entity.RoleMenuKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RoleMenuMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table role_menu
     *
     * @mbg.generated Mon Mar 28 11:37:08 CST 2022
     */
    long countByExample(RoleMenuExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table role_menu
     *
     * @mbg.generated Mon Mar 28 11:37:08 CST 2022
     */
    int deleteByExample(RoleMenuExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table role_menu
     *
     * @mbg.generated Mon Mar 28 11:37:08 CST 2022
     */
    int deleteByPrimaryKey(RoleMenuKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table role_menu
     *
     * @mbg.generated Mon Mar 28 11:37:08 CST 2022
     */
    int insert(RoleMenuKey record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table role_menu
     *
     * @mbg.generated Mon Mar 28 11:37:08 CST 2022
     */
    int insertSelective(RoleMenuKey record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table role_menu
     *
     * @mbg.generated Mon Mar 28 11:37:08 CST 2022
     */
    List<RoleMenuKey> selectByExample(RoleMenuExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table role_menu
     *
     * @mbg.generated Mon Mar 28 11:37:08 CST 2022
     */
    int updateByExampleSelective(@Param("record") RoleMenuKey record, @Param("example") RoleMenuExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table role_menu
     *
     * @mbg.generated Mon Mar 28 11:37:08 CST 2022
     */
    int updateByExample(@Param("record") RoleMenuKey record, @Param("example") RoleMenuExample example);
}