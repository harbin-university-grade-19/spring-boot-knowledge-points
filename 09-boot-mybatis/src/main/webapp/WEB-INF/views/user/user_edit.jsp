<%--
  Created by IntelliJ IDEA.
  User: jshand
  Date: 2022-04-01
  Time: 15:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>用户编辑</title>
    <style>
        .title{
            background-color: #F5F5F5;
            color: #000000;
            width: 200px;
            text-align: right;
            padding-right: 20px;
        }

        /*.content{*/

        /*}*/

    </style>
</head>
<body>
<%--用户编辑--%>

<div  > 出错的原因： ${msg}</div>

    <form action="${pageContext.request.contextPath}/user/save" method="post">
        <input type="hidden" name="userId" value="${user.userId}"/>
        <table cellpadding="0" cellspacing="0" border="1" width="100%">
            <tr>
                <td class="title">用户名</td>
                <td class="content"> <input type="text" name="username" value="${user.username}"/> </td>
            </tr>
            <tr>
                <td class="title">密码</td>
                <td class="content"> <input type="text" name="password" value="${user.password}"/> </td>
            </tr>
            <tr>
                <td class="title">昵称</td>
                <td class="content"> <input type="text" name="realname" value="${user.realname}"/> </td>
            </tr>
            <tr>
                <td class="title">电话号</td>
                <td class="content"> <input type="text" name="telephone" value="${user.telephone}" /> </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button type="submit">保存</button>
                </td>
            </tr>
        </table>





    </form>





</body>
</html>
