package com.neuedu.boot.service;

import com.neuedu.boot.entity.CheckItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 检查项目 服务类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface ICheckItemService extends IService<CheckItem> {

}
