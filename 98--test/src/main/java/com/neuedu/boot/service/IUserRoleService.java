package com.neuedu.boot.service;

import com.neuedu.boot.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色 服务类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface IUserRoleService extends IService<UserRole> {

}
