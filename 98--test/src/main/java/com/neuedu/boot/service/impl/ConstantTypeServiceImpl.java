package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.ConstantType;
import com.neuedu.boot.mapper.ConstantTypeMapper;
import com.neuedu.boot.service.IConstantTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 常数类别 服务实现类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Service
public class ConstantTypeServiceImpl extends ServiceImpl<ConstantTypeMapper, ConstantType> implements IConstantTypeService {

}
