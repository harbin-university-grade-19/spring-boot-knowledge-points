package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.StuTeacherRela;
import com.neuedu.boot.mapper.StuTeacherRelaMapper;
import com.neuedu.boot.service.IStuTeacherRelaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Service
public class StuTeacherRelaServiceImpl extends ServiceImpl<StuTeacherRelaMapper, StuTeacherRela> implements IStuTeacherRelaService {

}
