package com.neuedu.boot.service;

import com.neuedu.boot.entity.StuTeacherRela;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface IStuTeacherRelaService extends IService<StuTeacherRela> {

}
