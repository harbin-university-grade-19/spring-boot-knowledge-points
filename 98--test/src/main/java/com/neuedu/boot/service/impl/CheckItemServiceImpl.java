package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.CheckItem;
import com.neuedu.boot.mapper.CheckItemMapper;
import com.neuedu.boot.service.ICheckItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 检查项目 服务实现类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Service
public class CheckItemServiceImpl extends ServiceImpl<CheckItemMapper, CheckItem> implements ICheckItemService {

}
