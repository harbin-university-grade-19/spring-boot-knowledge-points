package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.Department;
import com.neuedu.boot.mapper.DepartmentMapper;
import com.neuedu.boot.service.IDepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 科室 服务实现类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements IDepartmentService {

}
