package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.InspectApply;
import com.neuedu.boot.mapper.InspectApplyMapper;
import com.neuedu.boot.service.IInspectApplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 检验申请 服务实现类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Service
public class InspectApplyServiceImpl extends ServiceImpl<InspectApplyMapper, InspectApply> implements IInspectApplyService {

}
