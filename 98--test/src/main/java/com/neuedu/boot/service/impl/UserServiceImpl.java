package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.User;
import com.neuedu.boot.mapper.UserMapper;
import com.neuedu.boot.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医生-用户信息 服务实现类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
