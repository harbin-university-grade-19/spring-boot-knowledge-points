package com.neuedu.boot.service;

import com.neuedu.boot.entity.Teacher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface ITeacherService extends IService<Teacher> {

}
