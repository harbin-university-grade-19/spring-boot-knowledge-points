package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.Teacher;
import com.neuedu.boot.mapper.TeacherMapper;
import com.neuedu.boot.service.ITeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements ITeacherService {

}
