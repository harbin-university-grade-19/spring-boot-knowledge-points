package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.RegistLevel;
import com.neuedu.boot.mapper.RegistLevelMapper;
import com.neuedu.boot.service.IRegistLevelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 挂号级别 服务实现类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Service
public class RegistLevelServiceImpl extends ServiceImpl<RegistLevelMapper, RegistLevel> implements IRegistLevelService {

}
