package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.Account;
import com.neuedu.boot.mapper.AccountMapper;
import com.neuedu.boot.service.IAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {

}
