package com.neuedu.boot.service;

import com.neuedu.boot.entity.CheckApply;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 检查申请 服务类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface ICheckApplyService extends IService<CheckApply> {

}
