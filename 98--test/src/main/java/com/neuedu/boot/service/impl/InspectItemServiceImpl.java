package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.InspectItem;
import com.neuedu.boot.mapper.InspectItemMapper;
import com.neuedu.boot.service.IInspectItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 检验项目 服务实现类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Service
public class InspectItemServiceImpl extends ServiceImpl<InspectItemMapper, InspectItem> implements IInspectItemService {

}
