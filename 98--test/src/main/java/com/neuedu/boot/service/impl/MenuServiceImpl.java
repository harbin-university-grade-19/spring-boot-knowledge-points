package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.Menu;
import com.neuedu.boot.mapper.MenuMapper;
import com.neuedu.boot.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单 服务实现类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

}
