package com.neuedu.boot.service;

import com.neuedu.boot.entity.RegistLevel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 挂号级别 服务类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface IRegistLevelService extends IService<RegistLevel> {

}
