package com.neuedu.boot.service;

import com.neuedu.boot.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 医生-用户信息 服务类
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface IUserService extends IService<User> {

}
