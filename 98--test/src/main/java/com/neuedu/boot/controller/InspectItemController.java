package com.neuedu.boot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 检验项目 前端控制器
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Controller
@RequestMapping("/inspectItem")
public class InspectItemController {

}
