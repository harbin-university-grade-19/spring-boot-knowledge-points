package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单 Mapper 接口
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface MenuMapper extends BaseMapper<Menu> {

}
