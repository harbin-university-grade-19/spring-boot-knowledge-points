package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.ConstantItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 常数项表 Mapper 接口
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface ConstantItemMapper extends BaseMapper<ConstantItem> {

}
