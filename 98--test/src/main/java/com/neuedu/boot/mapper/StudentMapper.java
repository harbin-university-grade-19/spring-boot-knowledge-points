package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface StudentMapper extends BaseMapper<Student> {

}
