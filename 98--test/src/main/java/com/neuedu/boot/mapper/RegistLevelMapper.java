package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.RegistLevel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 挂号级别 Mapper 接口
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface RegistLevelMapper extends BaseMapper<RegistLevel> {

}
