package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.Register;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 诊疗信息 Mapper 接口
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface RegisterMapper extends BaseMapper<Register> {

}
