package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.InspectItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 检验项目 Mapper 接口
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface InspectItemMapper extends BaseMapper<InspectItem> {

}
