package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.CheckItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 检查项目 Mapper 接口
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
public interface CheckItemMapper extends BaseMapper<CheckItem> {

}
