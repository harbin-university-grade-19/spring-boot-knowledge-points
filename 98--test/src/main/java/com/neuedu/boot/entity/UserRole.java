package com.neuedu.boot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户角色
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Getter
@Setter
@TableName("user_role")
public class UserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 医生id
     */
    private Integer userId;

    /**
     * 角色id
     */
    private Integer roleId;


}
