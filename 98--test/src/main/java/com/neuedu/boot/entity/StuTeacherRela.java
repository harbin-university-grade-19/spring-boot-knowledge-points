package com.neuedu.boot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 金山
 * @since 2022-04-01
 */
@Getter
@Setter
@TableName("stu_teacher_rela")
public class StuTeacherRela implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer teacherId;

    private Integer stuId;


}
