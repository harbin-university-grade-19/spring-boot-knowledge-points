package com.neuedu.boot.config;

import com.neuedu.boot.convert.String2DateConvert;
import com.neuedu.boot.intecerptor.MyHandlerInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AppConfig implements WebMvcConfigurer {


    /**
     * 用于添加Convert 或者Formatter
     * @param registry
     */
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new String2DateConvert());
    }


    /**
     * 添加拦截
     * @param registry
     */
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(new MyHandlerInterceptor()).addPathPatterns("/**");
//    }
//
//
//    @Override
//    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/index").setViewName("/index");
//        registry.addViewController("/test").setViewName("/test");
//    }


//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//
//
//        //http://localhost:8080/imgs/a.jpg
////        registry.
////                //spring.mvc.static-path-pattern
////                addResourceHandler("/imgs/**").
////                //spring.resources.static-locations
////                addResourceLocations("classpath:/images");
//
//
//        registry.addResourceHandler("/imgs/**").
//                addResourceLocations("classpath:/imgs/",
//                        "classpath:/mystatic/" ,
//                        "classpath:/static/" ,
//                        "classpath:/public/" ,
//                        "classpath:/META-INF/resources",
//                        "classpath:/resources");
//
//
//    }


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/js/**")
                .addResourceLocations("classpath:/js/");
    }
}
