package com.neuedu.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class JspController {

    @RequestMapping("/tojsp")
    String tojsp(){
//        return "/myjsp.jsp";
        return "myjsp";
    }

}
