package com.neuedu.boot.controller;

import com.neuedu.boot.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    Logger logger = LoggerFactory.getLogger(UserController.class);


    @RequestMapping("/update")
    User update(User user){
        logger.info(user.toString());
//        System.out.println("adsafas");
        return user;
    }

}
