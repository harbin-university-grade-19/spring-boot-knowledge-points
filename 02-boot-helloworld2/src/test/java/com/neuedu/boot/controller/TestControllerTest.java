package com.neuedu.boot.controller;

import com.neuedu.boot.App;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes= App.class)
@AutoConfigureMockMvc
        //相当于是使用 context 上下文构造一个 mvc对象
class TestControllerTest {


    @Autowired
    MockMvc mvc;

    @Test
    void test1() throws Exception {

        //模拟浏览器
        MvcResult result = mvc.perform(
                MockMvcRequestBuilders.get("/test/helloworld").
                        accept(MediaType.APPLICATION_JSON)).

                //期望 的 http状态
                andExpect(MockMvcResultMatchers.status().isOk()).

                //打印到控制台
                andDo(MockMvcResultHandlers.print()).
                //返回执行的结果
                andReturn();


    }
}