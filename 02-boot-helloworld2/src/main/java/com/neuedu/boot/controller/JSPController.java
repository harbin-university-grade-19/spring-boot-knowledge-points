package com.neuedu.boot.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class JSPController {

    @RequestMapping("/tojsp")
    public String tojsp(){
        return  "hello.jsp";
    }

}
