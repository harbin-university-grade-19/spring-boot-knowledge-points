package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.Menu;
import com.neuedu.boot.service.IMenuService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class MenuServiceImplTest {
    @Autowired
    IMenuService menuService;


    @Test
    void queryAll() {

        List<Menu> menus = menuService.queryAll();

        System.out.println(menus);


    }

    @Test
    void queryUserPowerMenu() {
        Integer id = 2; //root
//        Integer id = 6;  //admin
        List<Menu> menus = menuService.queryUserPowerMenu(id);
        for (Menu menu : menus) {
            System.out.println("menu = " + menu);
        }
    }
}