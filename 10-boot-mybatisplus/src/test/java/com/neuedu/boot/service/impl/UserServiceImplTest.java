package com.neuedu.boot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neuedu.boot.entity.User;
import com.neuedu.boot.service.IUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.ObjectUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserServiceImplTest {

    @Autowired
    IUserService userService;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void test(){


        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
//        if (user != null && !ObjectUtils.isEmpty(user.getUsername())) {
//            queryWrapper.like("username", user.getUsername());
//        }

        Page page = new Page();
        page.setCurrent(2);
        page.setSize(10);

        Page page1 = userService.page(page);
        List records = page1.getRecords();
        long total = page1.getTotal();
        long pages = page1.getPages();
        System.out.println("total = " + total);
        System.out.println("pages = " + pages);
        System.out.println("getCurrent = " + page1.getCurrent());
        System.out.println("size = " + page1.getSize());
        System.out.println(records);

    }
    
    
    @Test
    public void test2() throws JsonProcessingException {
//        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "{\"userId\":100}";

        // 第2步：反序列化JSON到对象。
        // 从JSON对象使用readValue()方法来获取。通过JSON字符串和对象类型作为参数JSON字符串/来源。
        //map json to student
        User student = objectMapper.readValue(jsonString, User.class);
        System.out.println(student);

    }





}