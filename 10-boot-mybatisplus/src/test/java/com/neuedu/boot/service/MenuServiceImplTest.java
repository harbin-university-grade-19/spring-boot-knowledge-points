package com.neuedu.boot.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.neuedu.boot.entity.Menu;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.ObjectUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class MenuServiceImplTest {


    @Autowired
    IMenuService menuService;


    @Test
    public void test1(){
        Menu menu = new Menu();
        menu.setParentId(2);
        //参数
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<Menu>();


        if(menu==null || menu.getParentId() == null){
            queryWrapper.isNull("parent_id");
        }
        if (menu != null && !ObjectUtils.isEmpty(menu.getParentId())) {
            queryWrapper.eq("parent_id",menu.getParentId());//定级菜单
        }



        if (menu != null && !ObjectUtils.isEmpty(menu.getMenuName())) {
            queryWrapper.like("menu_name", menu.getMenuName());
        }
        List<Menu> list = menuService.list(queryWrapper);

    }





}