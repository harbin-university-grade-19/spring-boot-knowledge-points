package com.neuedu.boot;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;

//@SpringBootTest
class ApplicationTests {

	@Test
	void contextLoads() {

		String url = "jdbc:mysql://127.0.0.1:3306/ssm_java4?useSSL=false";
		String username ="root";
		String password ="root";

		FastAutoGenerator.create(url, username, password)
				.globalConfig(builder -> {
					builder.author("张金山") // 设置作者
//							.enableSwagger() // 开启 swagger 模式
							.fileOverride() // 覆盖已生成文件
							.outputDir("E:\\development\\hrb19_java4\\springboot-parent-java4\\10-boot-mybatisplus\\src\\main\\java"); // 指定输出目录
				})
				.packageConfig(builder -> {
					builder.parent("com.neuedu.boot") // 设置父包名
//							.moduleName("system") // 设置父包模块名
							.pathInfo(Collections.singletonMap(OutputFile.xml, "E:\\development\\hrb19_java4\\springboot-parent-java4\\10-boot-mybatisplus\\src\\main\\resources\\com\\neuedu\\boot")); // 设置mapperXml生成路径
				})
				.strategyConfig(builder -> {
					builder.entityBuilder().enableLombok();//让实体支持lombok
					builder.addInclude("customer"); // 设置需要生成的表名
//							.addTablePrefix("t_", "c_"); // 设置过滤表前缀
				})
				.templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
				.execute();
	}

}
