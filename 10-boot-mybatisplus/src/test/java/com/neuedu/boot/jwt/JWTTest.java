package com.neuedu.boot.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.junit.jupiter.api.Test;

public class JWTTest {

    /**
     * 模拟的是登录的 生成jwt json
     */
    @Test
    public void testCreate(){
        String secret = "abcdef";
        String jwtString = JWT.create().
                withClaim("username", "root").  //设置登录的信息
                sign(Algorithm.HMAC256(secret));
        // admin
        // eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImFkbWluIn0.AwXJiXuMoJodNCq3EslfV6wVrwLIFFcaFqsAsWfITQg

        // root
        // eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InJvb3QifQ.c8kyOTkxOll2LM2_cEJpV2m1VtxSkkGyGET9F1WfqVE
        System.out.println(jwtString);


    }



    @Test
    public void decode(){
        //admin
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImFkbWluIn0.AwXJiXuMoJodNCq3EslfV6wVrwLIFFcaFqsAsWfITQg";
        DecodedJWT decode = JWT.decode(token);
        String username = decode.getClaim("username").asString();
        System.out.println("username = " + username);



        //root
        token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InJvb3QifQ.c8kyOTkxOll2LM2_cEJpV2m1VtxSkkGyGET9F1WfqVE";
        decode = JWT.decode(token);
        username = decode.getClaim("username").asString();
        System.out.println("username = " + username);
    }


}
