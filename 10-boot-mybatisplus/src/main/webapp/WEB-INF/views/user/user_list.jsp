<%--
  Created by IntelliJ IDEA.
  User: jshand
  Date: 2022-04-01
  Time: 15:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Title</title>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script>
        function rmById(id){

            let url = "${pageContext.request.contextPath}/user/ajaxDel"
            $.ajax(url,{
                type:"POST",
                dataType:'json',
                data:{
                    id:id
                },

                success:function(data){
                    if(data && data.success){
                        alert("删除成功")
                        window.location = window.location;
                    }else{
                        alert("删除失败")
                    }
                }
            })








        }
    </script>
</head>
<body>

用户列表  <a href="${pageContext.request.contextPath}/user/toEdit">添加</a>
<div>
    <form action="${pageContext.request.contextPath}/user/list">
        <input type="text" name="username" value="${user.username}" placeholder="请输入用户名" />
        <button type="submit">搜索</button>
    </form>
</div>


<table width="100%" cellpadding="0" cellspacing="0" border="1">
    <tr>
        <th>序号</th>
        <th>用户名</th>
        <th>昵称</th>
        <th>密码</th>
        <th>最后一次登录时间</th>
        <th>创建时间</th>
        <th>操作</th>
    </tr>
    <c:forEach items="${userList}" var="user" varStatus="stat">
        <tr>
            <td>${stat.count}</td>
            <td>${user.username}</td>
            <td>${user.realname}</td>
            <td>******</td>
            <td>
                    ${user.lastlogin}
            <td>
                ${user.createtime}
            </td>
            <td>
                <a href="${pageContext.request.contextPath}/user/toEdit?id=${user.userId}">修改</a> &nbsp;
                <a href="${pageContext.request.contextPath}/user/del?id=${user.userId}">删除(非AJax)</a> &nbsp;

                <button onclick="rmById(${user.userId})">删除(ajax )</button>
            </td>
        </tr>
    </c:forEach>


</table>


</body>
</html>
