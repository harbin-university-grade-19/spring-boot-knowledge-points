package com.neuedu.boot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.neuedu.boot.entity.Menu;
import com.neuedu.boot.entity.RoleMenu;
import com.neuedu.boot.mapper.MenuMapper;
import com.neuedu.boot.mapper.RoleMenuMapper;
import com.neuedu.boot.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 菜单 服务实现类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Autowired
    RoleMenuMapper roleMenuMapper;


    @Override
    public List<Menu> queryAll() {

        //顶层菜单（children）
        List<Menu> result = new ArrayList();
        MenuMapper baseMapper = getBaseMapper();

        //所有的菜单
        List<Menu> list = baseMapper.selectList(null);
        for (Menu menu : list) {
            //查找是否是顶层菜单
            if(menu.getParentId() == null){
                result.add(menu);
            }
        }
        
        //构造每个顶层菜单的 children
        for (Menu topMenu : result) {
            dealChildren(topMenu,list);//处理每个 top的 children
        }
        return result;
    }

    @Override
    public boolean grant(Integer roleId, Integer[] menuId) {

        //操作 role_menu表
        //先删后插入

        //根据roleId删除数据
        UpdateWrapper wrapper= new UpdateWrapper();
        wrapper.eq("role_id",roleId);
        int s1 = roleMenuMapper.delete(wrapper);
        boolean success = true;
        for (Integer mid : menuId) {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setRoleId(roleId);
            roleMenu.setMenuId(mid);
            int s2 = roleMenuMapper.insert(roleMenu);
            if(s2<=0){
                success = false;
                break;
            }
        }
        return success;
    }

    @Override
    public List<Integer> queryGrant(Integer roleId) {

        List<Integer> list = new ArrayList();

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("role_id",roleId);
        List<RoleMenu> roleMenus = roleMenuMapper.selectList(wrapper);

        for (RoleMenu roleMenu : roleMenus) {
            list.add(roleMenu.getMenuId());
        }

        return list;
    }

    /**
     * 根据用户查询用友的菜单权限
     * @param userId
     * @return
     */
    @Override
    public List<Menu> queryUserPowerMenu(Integer userId) {


        //顶层菜单（children）
        List<Menu> result = new ArrayList();
        MenuMapper baseMapper = getBaseMapper();

        //所有(有权限)的菜单
//        List<Menu> list = baseMapper.selectList(null);
        List<Menu> list = getBaseMapper().queryUserPowerMenu(userId);

        for (Menu menu : list) {
            //查找是否是顶层菜单
            if(menu.getParentId() == null){
                result.add(menu);
            }
        }

        //构造每个顶层菜单的 children
        for (Menu topMenu : result) {
            dealChildren(topMenu,list);//处理每个 top的 children
        }
        return result;
    }


    private void dealChildren(Menu menu , List<Menu> list){
        for (Menu child : list) {
            if(menu.getMenuId().equals(child.getParentId())){
                menu.getChildren().add(child);

                //处理child的 child
                dealChildren(child ,list );
            }
        }
    }




}
