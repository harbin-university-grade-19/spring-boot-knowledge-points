package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.Customer;
import com.neuedu.boot.mapper.CustomerMapper;
import com.neuedu.boot.service.ICustomerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户 服务实现类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-08
 */
@Service
public class CustomerServiceImpl extends ServiceImpl<CustomerMapper, Customer> implements ICustomerService {

}
