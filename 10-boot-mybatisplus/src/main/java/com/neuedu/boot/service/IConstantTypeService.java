package com.neuedu.boot.service;

import com.neuedu.boot.entity.ConstantType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 常数类别 服务类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface IConstantTypeService extends IService<ConstantType> {

}
