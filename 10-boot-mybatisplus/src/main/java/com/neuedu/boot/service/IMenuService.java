package com.neuedu.boot.service;

import com.neuedu.boot.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 菜单 服务类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface IMenuService extends IService<Menu> {



    List<Menu> queryAll();


    boolean grant(Integer roleId, Integer[] menuId);

    List<Integer> queryGrant(Integer roleId);


    /**
     * 根据用户查询所有的菜单权限
     * @param userId
     * @return
     */
    List<Menu> queryUserPowerMenu(Integer userId);




}
