package com.neuedu.boot.service;

import com.neuedu.boot.entity.InspectItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 检验项目 服务类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface IInspectItemService extends IService<InspectItem> {

}
