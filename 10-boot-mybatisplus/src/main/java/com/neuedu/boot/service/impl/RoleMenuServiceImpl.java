package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.RoleMenu;
import com.neuedu.boot.mapper.RoleMenuMapper;
import com.neuedu.boot.service.IRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色菜单信息 服务实现类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements IRoleMenuService {

}
