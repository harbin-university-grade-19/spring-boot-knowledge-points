package com.neuedu.boot.service;

import com.neuedu.boot.entity.RoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色菜单信息 服务类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface IRoleMenuService extends IService<RoleMenu> {

}
