package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.Register;
import com.neuedu.boot.mapper.RegisterMapper;
import com.neuedu.boot.service.IRegisterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诊疗信息 服务实现类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Service
public class RegisterServiceImpl extends ServiceImpl<RegisterMapper, Register> implements IRegisterService {

}
