package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.CheckApply;
import com.neuedu.boot.mapper.CheckApplyMapper;
import com.neuedu.boot.service.ICheckApplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 检查申请 服务实现类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Service
public class CheckApplyServiceImpl extends ServiceImpl<CheckApplyMapper, CheckApply> implements ICheckApplyService {

}
