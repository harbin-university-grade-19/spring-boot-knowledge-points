package com.neuedu.boot.service;

import com.neuedu.boot.entity.Register;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 诊疗信息 服务类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface IRegisterService extends IService<Register> {

}
