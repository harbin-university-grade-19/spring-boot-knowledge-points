package com.neuedu.boot.service;

import com.neuedu.boot.entity.ConstantItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 常数项表 服务类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface IConstantItemService extends IService<ConstantItem> {

}
