package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.Student;
import com.neuedu.boot.mapper.StudentMapper;
import com.neuedu.boot.service.IStudentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {

}
