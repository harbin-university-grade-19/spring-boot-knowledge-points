package com.neuedu.boot.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.neuedu.boot.entity.User;
import com.neuedu.boot.entity.UserRole;
import com.neuedu.boot.mapper.UserMapper;
import com.neuedu.boot.mapper.UserRoleMapper;
import com.neuedu.boot.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医生-用户信息 服务实现类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    UserRoleMapper roleMapper;


    @Override
    public boolean grant(Integer userId, Integer[] roleIds) throws Exception {
        //先删除原有数据
        UpdateWrapper wrapper = new UpdateWrapper();
        wrapper.eq("user_id",userId);
        roleMapper.delete(wrapper);

        for (Integer roleId : roleIds) {
            UserRole userRole = new UserRole();
            userRole.setUserId(userId);
            userRole.setRoleId(roleId);

            int count = roleMapper.insert(userRole);
            if(count<=0){
                throw new Exception("授权失败");
            }

        }

        return true;
    }
}
