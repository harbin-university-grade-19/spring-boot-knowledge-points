package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.ConstantItem;
import com.neuedu.boot.mapper.ConstantItemMapper;
import com.neuedu.boot.service.IConstantItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 常数项表 服务实现类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Service
public class ConstantItemServiceImpl extends ServiceImpl<ConstantItemMapper, ConstantItem> implements IConstantItemService {

}
