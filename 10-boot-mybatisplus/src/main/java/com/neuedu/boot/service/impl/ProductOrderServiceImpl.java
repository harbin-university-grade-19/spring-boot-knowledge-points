package com.neuedu.boot.service.impl;

import com.neuedu.boot.entity.ProductOrder;
import com.neuedu.boot.mapper.ProductOrderMapper;
import com.neuedu.boot.service.IProductOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Service
public class ProductOrderServiceImpl extends ServiceImpl<ProductOrderMapper, ProductOrder> implements IProductOrderService {

}
