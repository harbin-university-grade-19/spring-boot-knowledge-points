package com.neuedu.boot.service;

import com.neuedu.boot.entity.Customer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客户 服务类
 * </p>
 *
 * @author 张金山
 * @since 2022-04-08
 */
public interface ICustomerService extends IService<Customer> {

}
