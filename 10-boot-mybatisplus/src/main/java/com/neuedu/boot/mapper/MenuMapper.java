package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 菜单 Mapper 接口
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> queryUserPowerMenu(Integer userId);

}
