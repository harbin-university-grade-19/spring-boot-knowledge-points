package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 医生-用户信息 Mapper 接口
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface UserMapper extends BaseMapper<User> {

}
