package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色 Mapper 接口
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
