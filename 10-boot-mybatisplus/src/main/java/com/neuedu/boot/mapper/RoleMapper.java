package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface RoleMapper extends BaseMapper<Role> {

}
