package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.Customer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客户 Mapper 接口
 * </p>
 *
 * @author 张金山
 * @since 2022-04-08
 */
public interface CustomerMapper extends BaseMapper<Customer> {

}
