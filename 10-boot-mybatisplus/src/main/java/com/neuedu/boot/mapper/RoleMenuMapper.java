package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色菜单信息 Mapper 接口
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

}
