package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.InspectApply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 检验申请 Mapper 接口
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface InspectApplyMapper extends BaseMapper<InspectApply> {

}
