package com.neuedu.boot.mapper;

import com.neuedu.boot.entity.ConstantType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 常数类别 Mapper 接口
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
public interface ConstantTypeMapper extends BaseMapper<ConstantType> {

}
