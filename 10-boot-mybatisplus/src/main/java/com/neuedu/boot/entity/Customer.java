package com.neuedu.boot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 客户
 * </p>
 *
 * @author 张金山
 * @since 2022-04-08
 */
@Getter
@Setter
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 客户姓名
     */
    private String name;

    /**
     * 性别
     */
    private String gender;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 身份证号
     */
    private String idcard;

    /**
     * 房间号
     */
    private String roomno;

    /**
     * 所属楼房
     */
    private String floor;

    /**
     * 档案号
     */
    private String archives;

    /**
     * 入住时间
     */
    private LocalDateTime intime;

    /**
     * 合同到期时间
     */
    private LocalDateTime endtime;

    /**
     * 联系电话
     */
    private String telephone;

    /**
     * 负责医生
     */
    private String doctor;

    /**
     * 护工
     */
    private String nurse;

    /**
     * 1 是有效，0是无效
     */
    private String active;

    /**
     * 数据的创建时间
     */
    private LocalDateTime ctime;


}
