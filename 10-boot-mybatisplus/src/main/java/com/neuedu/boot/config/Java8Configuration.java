package com.neuedu.boot.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Configuration
public class Java8Configuration {


    @Bean
    Converter<String, LocalDateTime> string2DataTime(){

        return new Converter<String, LocalDateTime>() {
            @Override
            public LocalDateTime convert(String source) {

                //yyyy-MM-dd HH:mm:ss
                return LocalDateTime.parse(source, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            }
        };

    }

    @Bean
    Converter<String, LocalDate> string2Data(){

        return new Converter<String, LocalDate>() {
            @Override
            public LocalDate convert(String source) {

                //yyyy-MM-dd
                return LocalDate.parse(source, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            }
        };

    }


    public static void main(String[] args) {
        String source = "2022-04-07 02:02:02";
        LocalDateTime ldt = LocalDateTime.parse(source, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        System.out.println(ldt);

    }


}
