package com.neuedu.boot.config;

import com.neuedu.boot.sso.SSOInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SSOConfig implements WebMvcConfigurer {

    @Autowired
    SSOInterceptor ssoInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(ssoInterceptor).addPathPatterns("/**").excludePathPatterns("/login");
    }
}
