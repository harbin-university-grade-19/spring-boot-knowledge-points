package com.neuedu.boot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfiguration {



//    @Bean
//    @Primary
//    public RedisTemplate  createRedisTemplate(RedisConnectionFactory factory){
//        RedisTemplate redisTemplate = new RedisTemplate();
//
////        127.0.0.1 6379
//        redisTemplate.setConnectionFactory(factory);
//
//        //设置序列化方式的类
//        redisTemplate.setKeySerializer(new StringRedisSerializer());
//        redisTemplate.setValueSerializer(new StringRedisSerializer());
//
//        redisTemplate.setHashKeySerializer(new StringRedisSerializer() );
//        redisTemplate.setHashValueSerializer(new StringRedisSerializer() );
//
//        redisTemplate.afterPropertiesSet();
//
//        return redisTemplate;
//    }


    @Bean
    @Primary
    public RedisTemplate  createRedisTemplate( RedisTemplate redisTemplate){

        //设置序列化方式的类
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new StringRedisSerializer());

        redisTemplate.setHashKeySerializer(new StringRedisSerializer() );
        redisTemplate.setHashValueSerializer(new StringRedisSerializer() );

        return redisTemplate;
    }

}
