package com.neuedu.boot.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 全局的异常处理器
 */
@RestControllerAdvice
public class MyExceptionResolver {


    @ExceptionHandler()
    public CommonResult resolver(HttpServletRequest request, HttpServletResponse response ,Exception ex){
        //404
        if(ex instanceof NoHandlerFoundException){
            return CommonResult.nohandler();
        }
        //500
        return CommonResult.failed(ex.getMessage());

    }

}
