package com.neuedu.boot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neuedu.boot.config.CommonResult;
import com.neuedu.boot.entity.Role;
import com.neuedu.boot.entity.User;
import com.neuedu.boot.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 医生-用户信息 前端控制器
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    IUserService userService;


    //    //列表
//    @RequestMapping("/list")
//    String list(User user, Model model) {
//
//        //参数
//        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
//        if(user!=null && !ObjectUtils.isEmpty(user.getUsername())){
//            queryWrapper.like("username",user.getUsername());
//        }
//
//        List<User> list = userService.list(queryWrapper );
//
//
//        model.addAttribute("userList", list);
//
//        //配置前缀  /WEB-INF/views/
//        //后缀    .jsp
//        return "user/user_list";   //  /WEB-INF/views/user/user_list.jsp
//}





    @RequestMapping("/list")
    @ResponseBody
    CommonResult list(User user) {

        //参数
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if (user != null && !ObjectUtils.isEmpty(user.getUsername())) {
            queryWrapper.like("username", user.getUsername());
        }
        List<User> list = userService.list(queryWrapper);

        return CommonResult.success(list);

    }

    @RequestMapping("/page")
    @ResponseBody
    CommonResult page(Page page , User user) {

        //参数
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if (user != null && !ObjectUtils.isEmpty(user.getUsername())) {
            queryWrapper.like("username", user.getUsername());
        }
        page = userService.page(page,queryWrapper);

        return CommonResult.success(page);

    }


    //列表


    /**
     * 有id的即为 编辑
     * 否则添加
     *
     * @return
     */
    @RequestMapping("/toEdit")
    String toEdit(Integer id, Model model) {
        //如果有id，查询数据
        if (!ObjectUtils.isEmpty(id)) {
            User user = userService.getById(id);
            model.addAttribute("user", user);
        }


        return "user/user_edit";
    }


    @RequestMapping("/getById")
    @ResponseBody
    CommonResult getById( Integer id) {


        User user = userService.getById(id);

        return CommonResult.success(user);

    }



    //去添加 or 修改
    @PostMapping("/save")
    String save(User user) {


        boolean success = userService.saveOrUpdate(user);

//        boolean success = userService.save(user);

        if (success) {
            return "redirect:/user/list";
        } else {

            return "/user/toEdit";
        }

    }


    //根据主键删除
    @GetMapping("/del")
    String del(Integer id, Model model) {

        boolean success = userService.removeById(id);

        if (success) {
            return "redirect:/user/list";
        } else {

            return "/user/list";
        }

    }


    //根据主键删除
    @PostMapping("/ajaxDel")
    @ResponseBody
    Map ajaxDel(Integer id, Model model) {

        boolean success = userService.removeById(id);

        Map map = new HashMap();
        map.put("success", success);

        return map;
    }



    //根据主键删除
    @PostMapping("/grant")
    @ResponseBody
    CommonResult grant(Integer userId, Integer[] roleIds) throws Exception {

        boolean success =userService.grant(userId,roleIds);

        return  CommonResult.success(success);
    }




}
