package com.neuedu.boot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neuedu.boot.config.CommonResult;
import com.neuedu.boot.entity.Customer;
import com.neuedu.boot.entity.User;
import com.neuedu.boot.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 客户 前端控制器
 * </p>
 *
 * @author 张金山
 * @since 2022-04-08
 */
@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    ICustomerService customerService;


    @RequestMapping("/page")
    @ResponseBody
    CommonResult page(Page page , Customer customer) {

        //参数
        QueryWrapper<Customer> queryWrapper = new QueryWrapper<>();
        if (customer != null && !ObjectUtils.isEmpty(customer.getName())) {
            queryWrapper.like("name", customer.getName());
        }
        page = customerService.page(page,queryWrapper);

        return CommonResult.success(page);

    }

    @RequestMapping("/save")
    @ResponseBody
    CommonResult save( Customer customer) {
        boolean success = customerService.saveOrUpdate(customer);
        return CommonResult.success(success);
    }
    @RequestMapping("/getById")
    @ResponseBody
    CommonResult getById( Integer id) {


        Customer customer = customerService.getById(id);

        return CommonResult.success(customer);

    }


    @RequestMapping("/deleteById")
    @ResponseBody
    CommonResult deleteById( Integer id) {

        boolean success = customerService.removeById(id);

        return CommonResult.success(success);

    }


    public static void main(String args[]){
        int numericValue = Character.getNumericValue('8');
        System.out.println(numericValue);
    }

}
