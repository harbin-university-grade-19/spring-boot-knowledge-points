package com.neuedu.boot.controller;

import com.neuedu.boot.config.CommonResult;
import com.neuedu.boot.service.IRegistLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 挂号级别 前端控制器
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Controller
@RequestMapping("/registLevel")
public class RegistLevelController {

    @Autowired
    IRegistLevelService registLevelService;

    @RequestMapping("/list")
    @ResponseBody
    CommonResult list( ) {
        return CommonResult.success(registLevelService.list());
    }

}
