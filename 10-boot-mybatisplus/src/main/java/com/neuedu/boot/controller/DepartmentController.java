package com.neuedu.boot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neuedu.boot.config.CommonResult;
import com.neuedu.boot.entity.User;
import com.neuedu.boot.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 科室 前端控制器
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@Controller
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    IDepartmentService departmentService;


    @RequestMapping("/list")
    @ResponseBody
    CommonResult page( ) {

        return CommonResult.success(departmentService.list());

    }
    

}
