package com.neuedu.boot.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neuedu.boot.config.CommonResult;
import com.neuedu.boot.entity.Menu;
import com.neuedu.boot.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 菜单 前端控制器
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    IMenuService menuService;

    @RequestMapping("/page")
    CommonResult page(Page page , Menu menu) {

        //参数
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<Menu>();
        if (menu != null && !ObjectUtils.isEmpty(menu.getMenuName())) {
            queryWrapper.like("menu_name", menu.getMenuName());
        }
        page = menuService.page(page,queryWrapper);

        return CommonResult.success(page);

    }


    @RequestMapping("/list")
    CommonResult top(Page page , Menu menu) {

        QueryWrapper<Menu> queryWrapper = new QueryWrapper<Menu>();


        if(menu==null || menu.getParentId() == null){
            queryWrapper.isNull("parent_id");
        }
        if (menu != null && !ObjectUtils.isEmpty(menu.getParentId())) {
            queryWrapper.eq("parent_id",menu.getParentId());//定级菜单
        }


        if (menu != null && !ObjectUtils.isEmpty(menu.getMenuName())) {
            queryWrapper.like("menu_name", menu.getMenuName());
        }
        List<Menu> list = menuService.list(queryWrapper);

        return CommonResult.success(list);
    }



    @RequestMapping("queryAll")
    CommonResult queryAll(){
        List<Menu> menus = menuService.queryAll();
        return CommonResult.success(menus);
    }


    @RequestMapping("grant")
    CommonResult grant(Integer roleId, Integer[]  menuId){

        System.out.println("roleId = " + roleId);
        System.out.println("menuId = ");
        for (Integer mId : menuId) {
            System.out.println("mId = " + mId);
        }

       boolean success  = menuService.grant(roleId,menuId);

        return CommonResult.success(success);
    }


    @RequestMapping("queryGrant")
    CommonResult queryGrant(Integer roleId){

        List<Integer> menuIds = menuService.queryGrant(roleId);

        return CommonResult.success(menuIds);
    }

    @RequestMapping("queryUserPowerMenu")
    CommonResult queryUserPowerMenu(HttpServletRequest request){

        String token = request.getHeader("token");

        DecodedJWT decode = JWT.decode(token);
        Integer userId = decode.getClaim("userId").asInt();


        List<Menu> menuIds = menuService.queryUserPowerMenu(userId);

        return CommonResult.success(menuIds);
    }





    @RequestMapping("/save")
    CommonResult save( Menu menu) {
        boolean success = menuService.saveOrUpdate(menu);
        return CommonResult.success(success);
    }

    @RequestMapping("/getById")
    CommonResult getById( Integer id) {

        Menu menu = menuService.getById(id);
        return CommonResult.success(menu);
    }


    @RequestMapping("/deleteById")
    CommonResult deleteById( Integer id) {

        boolean success = menuService.removeById(id);
        return CommonResult.success(success);
    }


}
