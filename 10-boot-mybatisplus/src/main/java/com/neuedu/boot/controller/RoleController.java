package com.neuedu.boot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.neuedu.boot.config.CommonResult;
import com.neuedu.boot.entity.Role;
import com.neuedu.boot.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色 前端控制器
 * </p>
 *
 * @author 张金山
 * @since 2022-04-02
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    IRoleService roleService;

    @RequestMapping("/page")
    CommonResult page(Page page , Role role) {

        //参数
        QueryWrapper<Role> queryWrapper = new QueryWrapper<Role>();
        if (role != null && !ObjectUtils.isEmpty(role.getRoleName())) {
            queryWrapper.like("role_name", role.getRoleName());
        }
        page = roleService.page(page,queryWrapper);

        return CommonResult.success(page);

    }

    @RequestMapping("/save")
    @ResponseBody
    CommonResult save( Role role) {
        boolean success = roleService.saveOrUpdate(role);
        return CommonResult.success(success);
    }
    @RequestMapping("/getById")
    @ResponseBody
    CommonResult getById( Integer id) {


       Role role = roleService.getById(id);

        return CommonResult.success(role);

    }


    @RequestMapping("/deleteById")
    @ResponseBody
    CommonResult deleteById( Integer id) {

        boolean success = roleService.removeById(id);

        return CommonResult.success(success);

    }


}
