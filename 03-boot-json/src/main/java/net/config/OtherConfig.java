package net.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;
import java.util.Date;

@Configuration
public class OtherConfig {


    @Bean("currentTime")
    String  currentTime(){

        return new Date().toString();
    }



}
