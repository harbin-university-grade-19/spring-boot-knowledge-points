package com.neuedu.boot.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 实体类
 */
public class User {

    private Integer id;
    private String name;
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date  birthDay;

    public User() {
    }

    public User(Integer id, String name, Date createTime) {
        this.id = id;
        this.name = name;
        this.createTime = createTime;
    }

    public User(Integer id, String name, Date createTime, Date birthDay) {
        this.id = id;
        this.name = name;
        this.createTime = createTime;
        this.birthDay = birthDay;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", createTime=" + createTime +
                ", birthDay=" + birthDay +
                '}';
    }
}
