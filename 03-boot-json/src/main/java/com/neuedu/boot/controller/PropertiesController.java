package com.neuedu.boot.controller;

import net.config.MenuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//jdbc.properties
//@PropertySource("jdbc.properties")
//@PropertySource(value="jdbc.properties")
//读取属性文件
@PropertySource(value={"jdbc.properties"})
public class PropertiesController {

    @Value("${jdbc.url}")
    private String url;

    @Value("${jdbc.username}")
    private String username;

    @Value("${jdbc.password}")
    private String password;
    /**
     * 连接数据的程序
     * @return
     */
    @RequestMapping("/properties")
    String connectionDataSource(){
        //获取url、username、password
        //获取属性文件中的值

        System.out.println("url = " + url);
        System.out.println("username = " + username);
        System.out.println("password = " + password);
        //可以执行连接数据的程序

        return "url:"+url+",username:"+username+",password:"+password;
    }

    @Autowired
    @Qualifier(value="currentTime")
    String currentTime;


    @RequestMapping("/getCurrentTime")
    String getCurrentTime(){
        System.out.println("currentTime = " + currentTime);
        return currentTime;
    }



    @Autowired
    MenuDao menuDao;

    @RequestMapping("/xmlConfig")
    String xmlConfig(){

        System.out.println(menuDao);
        return menuDao.toString();
    }






}
