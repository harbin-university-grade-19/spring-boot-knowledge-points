package com.neuedu.boot.controller;


import com.neuedu.boot.entity.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.*;


/**
 * http://localhost:8080/json/string
 * http://localhost:8080/json/map
 * http://localhost:8080/json/entity
 * http://localhost:8080/json/list
 */
@RestController
@RequestMapping("/json")
public class JSONController {


    public JSONController() {
        System.out.println("2111");
    }

    /**
     * 将作为结果响应
     *
     * @return
     */
    @RequestMapping("/string")
    public String jString() {

        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        return time;
    }

    /**
     * 将作为结果响应
     *
     * @return
     */
    @RequestMapping("/map")
    Map map() {

        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        Map map = new HashMap();
        map.put("code", 200);
        map.put("msg", "无权访问");
        map.put("data", time);

        return map;
    }


    /**
     * 模拟根据主键查询
     * @return
     */
    @RequestMapping("/entity")
    User entity() {

        User user = new User(10, "张飞", new Date(),new Date());

        return user;
    }



    /**
     * 模拟根据主键查询
     * @return
     */
    @RequestMapping("/list")
    List<User> list() {
        List list = new ArrayList();
        for (int i = 0; i < 10; i++) {
            list.add(new User(10+i, "张飞"+i, new Date()));
        }

        return list;
    }

}
