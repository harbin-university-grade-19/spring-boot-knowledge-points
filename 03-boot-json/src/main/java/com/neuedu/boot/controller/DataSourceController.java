package com.neuedu.boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.sql.SQLException;

@RestController
public class DataSourceController {

    @Autowired(required = false)
    DataSource dataSource;


    @RequestMapping("/auto/datasource")
    String getSource() throws SQLException {
        System.out.println("dataSource = " + dataSource);
        System.out.println("dataSource.getConnection() = " + dataSource.getConnection());
        return dataSource.toString();
    }



}
