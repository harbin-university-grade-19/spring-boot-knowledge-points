package com.neuedu.boot;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neuedu.boot.dao.UserDao;
import com.neuedu.boot.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;

@Configuration
//@ComponentScan(basePackages = "cn.neuedu.other")
public class AppConfig {


    @Bean("userService")   //类似于 bean标签 ,没办法声明@Service注解，只能自定义类的构造过程，
    //bean 的名字默认是方法的名
    public UserService getUserService(DispatcherServlet dispatcherServlet){
        System.out.println("dispatcherServlet = " + dispatcherServlet);
//        System.out.println("context = " + context);
        return new UserService();
    }

}
