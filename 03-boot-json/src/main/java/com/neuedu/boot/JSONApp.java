package com.neuedu.boot;

import net.config.OtherConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

/**
 * APP作为启动类，也可以是 配置类
 */
//@SpringBootApplication(scanBasePackages = {"com.neuedu.boot","cn.neuedu.other"}
//,exclude = DataSourceAutoConfiguration.class)

@SpringBootConfiguration
@EnableAutoConfiguration(exclude=DataSourceAutoConfiguration.class)
@ComponentScan


@Import(value = {OtherConfig.class})
@ImportResource("classpath:spring-config.xml")

public class JSONApp {

    public static void main(String[] args) {
        SpringApplication.run(JSONApp.class,args);
    }



}
