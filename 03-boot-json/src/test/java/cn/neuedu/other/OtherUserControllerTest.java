package cn.neuedu.other;

import com.neuedu.boot.JSONApp;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = JSONApp.class)
class OtherUserControllerTest {

    @Autowired
    OtherUserController otherUserController;


    @Test
    void other() {
        otherUserController.other();
    }
}