package com.neuedu.boot.service;

import com.neuedu.boot.JSONApp;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
//@SpringBootTest(classes = JSONApp.class)
class UserServiceTest {

    @Autowired
    UserService userService;

    @Test
    void sayHi() {
        userService.sayHi();
    }
}